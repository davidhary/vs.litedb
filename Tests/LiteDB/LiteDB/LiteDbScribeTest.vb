Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports FluentAssertions
Imports LiteDB

''' <summary>
''' Contains all unit tests for the <see cref="LiteDB"></see>
''' class.
''' </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/21/2019 </para></remarks>
<TestClass()>
Public Class LiteDbScribeTest

#Region " CONSTRUCTION and CLEANUP "

	''' <summary> My class initialize. </summary>
	''' <param name="testContext"> Gets or sets the test context which provides information about
	'''                            and functionality for the current test run. </param>
	''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
	<CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
	<ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
        If _TestSite IsNot Nothing Then _TestSite.Dispose() : _TestSite = Nothing
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset).Split} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

	''' <summary> Cleans up after each test has run. </summary>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	'''<summary>
	'''Gets or sets the test context which provides
	'''information about and functionality for the current test run.
	'''</summary>
	Public Property TestContext() As TestContext

	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region "  STORE and SEARCH "

	''' <summary> A customer POCO Class. </summary>
	Public Class Customer
		Public Property Id() As Integer
		Public Property Name() As String
		Public Property Age() As Integer
		Public Property Phones() As String()
		Public Property IsActive() As Boolean
	End Class

	''' <summary> A quick example for storing and searching documents. </summary>
	<TestMethod(), Description("A quick example for storing and searching documents")>
	Public Sub StoreAndSearchTest()

		' Open database (or create if doesn't exist)
		Using db As New LiteDatabase("MyData.db")

			' Get customer collection
			Dim col As LiteCollection(Of Customer) = db.GetCollection(Of Customer)("customers")

			' Create your new customer instance
			Dim customer As New Customer With {.Name = "John Doe", .Phones = New String() {"8000-0000", "9000-0000"}, .Age = 39, .IsActive = True}

			' Create unique index in Name field
			col.EnsureIndex(Function(x) x.Name, True)

			' Insert new customer document (Id will be auto-incremented)
			col.Insert(customer)

			' Update a document inside a collection
			customer.Name = "Joana Doe"

			col.Update(customer)

			' Use LINQ to query documents (with no index)
			Dim results = col.Find(Function(x) x.Age > 20)

		End Using

	End Sub

#End Region

#Region " FLUENT MAPPER "

	Public Class Address
		Public Property AddressId() As Integer
		Public Property Streat() As String
		Public Property State() As String
		Public Property ZipCode() As String
		Public Property Country() As String
	End Class

	Public Class Product
		Public Property ProductId() As Integer
		Public Property Name() As String
		Public Property Price() As Decimal
	End Class

	Public Class Order
		<CLSCompliant(False)>
		Public Property Id() As ObjectId
		Public Property OrderDate() As Date
		Public Property ShippingAddress() As Address
		Public Property Customer() As Customer
		Public Property Products() As List(Of Product)
	End Class

	' Re-use mapper from global instance
	Private mapper As BsonMapper = BsonMapper.Global

	<TestMethod(), Description("Using fluent mapper and cross document reference for more complex data models")>
	<Obsolete("Requires creation of database")>
	Public Sub UsingFluentMapperTest()

		' "Products" and "Customer" are from other collections (not embedded document)
		' Embedded sub document -  1 to Many reference -  1 to 1/0 reference
		mapper.Entity(Of Order)().DbRef(Function(x) x.Customer, "customers").DbRef(Function(x) x.Products, "products").Field(Function(x) x.ShippingAddress, "addresses")

		Using db = New LiteDatabase("MyOrderDatafile.db")
			Dim orders = db.GetCollection(Of Order)("orders")

			' When query Order, includes references
			Dim query As IEnumerable(Of Order) = orders.Include(Function(x) x.Customer).Include(Function(x) x.Products).Find(Function(x) x.OrderDate <= DateTime.Now) ' 1 to many reference

			' Each instance of Order will load Customer/Products references
			For Each order In query
				Dim name As String = order.Customer.Name
				name.Should().NotBeNullOrWhiteSpace($"Fetch names should not be null")
			Next order
		End Using

	End Sub

#End Region

End Class
