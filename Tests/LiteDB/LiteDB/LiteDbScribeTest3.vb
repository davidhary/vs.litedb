Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports FluentAssertions
Imports LiteDB

''' <summary>
''' Contains all unit tests for the <see cref="LiteDB"></see>
''' class.
''' </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/21/2019 </para></remarks>
<TestClass()>
Public Class LiteDbScribeTest3

#Region " CONSTRUCTION and CLEANUP "

	''' <summary> My class initialize. </summary>
	''' <param name="testContext"> Gets or sets the test context which provides information about
	'''                            and functionality for the current test run. </param>
	''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
	<CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
	<ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
        If _TestSite IsNot Nothing Then _TestSite.Dispose() : _TestSite = Nothing
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset).Split} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

	''' <summary> Cleans up after each test has run. </summary>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	'''<summary>
	'''Gets or sets the test context which provides
	'''information about and functionality for the current test run.
	'''</summary>
	Public Property TestContext() As TestContext

	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " DB REF FOR  CROSS REFERENCES "

	' DbRef to cross references
	Public Class Customer
		Public Property Id() As Integer
		Public Property Name() As String
	End Class

	Public Class Product
		Public Property Id() As Integer
		Public Property Description() As String
		Public Property Price() As Double
	End Class

	' DbRef to cross references
	Public Class Order
		<CLSCompliant(False)>
		Public Property Id() As ObjectId
		Public Property OrderDate() As Date
		Public Property Customer() As Customer
		Public Property Products() As List(Of Product)
	End Class

	' Re-use mapper from global instance
	Private mapper As BsonMapper = BsonMapper.Global


	<Obsolete("not tested")>
	Public Sub CrossReferenceTest()

		' Products and Customer are other collections (not embedded document)
		' you can use [BsonRef("colname")] attribute
		mapper.Entity(Of Order)().DbRef(Function(x) x.Products, "products").DbRef(Function(x) x.Customer, "customers")


		' Open database (or create if doesn't exist)
		Using db As New LiteDatabase("MyData.db")

			Dim customers As LiteCollection(Of Customer) = db.GetCollection(Of Customer)("customers")
			Dim products As LiteCollection(Of Product) = db.GetCollection(Of Product)("products")
			Dim orders As LiteCollection(Of Order) = db.GetCollection(Of Order)("orders")

			' create examples
			Dim john = New Customer With {.Name = "John Doe"}
			Dim tv = New Product With {.Description = "TV Sony 44""", .Price = 799}
			Dim iphone = New Product With {.Description = "iPhone X", .Price = 999}
			Dim order1 = New Order With {.OrderDate = New Date(2017, 1, 1), .Customer = john, .Products = New List(Of Product) From {iphone, tv}}
			Dim order2 = New Order With {.OrderDate = New Date(2017, 10, 1), .Customer = john, .Products = New List(Of Product) From {iphone}}

			' insert into collections
			customers.Insert(john)
			products.Insert(New Product() {tv, iphone})
			orders.Insert(New Order() {order1, order2})

			' create index in OrderDate
			orders.EnsureIndex(Function(x) x.OrderDate)

			' When query Order, includes references
			Dim query = orders.Include(Function(x) x.Customer).Include(Function(x) x.Products).Find(Function(x) x.OrderDate = New Date(2017, 1, 1))

			' Each instance of Order will load Customer/Products references
			For Each c In query
				Console.WriteLine("#{0} - {1}", c.Id, c.Customer.Name)

				For Each p In c.Products
					Console.WriteLine(" > {0} - {1:c}", p.Description, p.Price)
				Next p
			Next c

		End Using

	End Sub

#End Region

#Region " STREAM A DATABASE "

	<Obsolete("not tested")>
	Public Sub StreamDatabaseTest()

		Dim mem As New System.IO.MemoryStream
		Using db As New LiteDatabase(mem)
			' ...
			' Get database as binary array
			Dim bytes As Byte() = mem.ToArray()

			' LiteDB support any Stream read/write as input
			' You can implement your own IDiskService to persist data
			' 
		End Using

	End Sub

#End Region

End Class
