Imports isr.Core.Services
''' <summary> A LiteDB Test Info. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class LiteDbScribeTestInfo
	Inherits isr.Core.Services.ApplicationSettingsBase

#Region " SINGLETON "

	Private Sub New()
		MyBase.New
	End Sub

	''' <summary> Gets the locking object to enforce thread safety when creating the singleton
	''' instance. </summary>
	''' <value> The sync locker. </value>
	Private Shared Property _SyncLocker As New Object

	''' <summary> Gets the instance. </summary>
	''' <value> The instance. </value>
	Private Shared Property _Instance As LiteDbScribeTestInfo

	''' <summary> Instantiates the class. </summary>
	''' <remarks> Use this property to instantiate a single instance of this class. This class uses
	''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
	''' <returns> A new or existing instance of the class. </returns>
	Public Shared Function [Get]() As LiteDbScribeTestInfo
		If _Instance Is Nothing Then
			SyncLock _SyncLocker
				_Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New LiteDbScribeTestInfo()), LiteDbScribeTestInfo)
			End SyncLock
		End If
		Return _Instance
	End Function

	''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
	''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
	Public Shared ReadOnly Property Instantiated() As Boolean
		Get
			SyncLock _SyncLocker
				Return _Instance IsNot Nothing
			End SyncLock
		End Get
	End Property

#End Region

#Region " SETTINGS EDITORS EXCLUDED "

	''' <summary> Opens the settings editor. </summary>
	Public Shared Sub OpenSettingsEditor()
		isr.Core.Services.WindowsForms.EditConfiguration($"{GetType(LiteDbScribeTestInfo)} Editor", LiteDbScribeTestInfo.Get)
	End Sub

#End Region

#Region " CONFIGURATION INFORMATION "

	''' <summary> Returns true if test settings exist. </summary>
	''' <value> <c>True</c> if testing settings exit. </value>
	<CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
	Public Property Exists As Boolean
		Get
			Return Me.AppSettingBoolean
		End Get
		Set
			Me.AppSettingBoolean = Value
		End Set
	End Property

	''' <summary> Returns true to output test messages at the verbose level. </summary>
	''' <value> The verbose messaging level. </value>
	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
	Public Property Verbose As Boolean
		Get
			Return Me.AppSettingBoolean
		End Get
		Set
			Me.AppSettingBoolean = Value
		End Set
	End Property

	''' <summary> Gets the sentinel indicating of all data are to be used for a test. </summary>
	''' <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
	<CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
	Public Property All As Boolean
		Get
			Return Me.AppSettingBoolean
		End Get
		Set
			Me.AppSettingBoolean = Value
		End Set
	End Property

	''' <summary> True if the test set is enabled. </summary>
	''' <value> The enabled option. </value>
	<CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
	Public Property Enabled As Boolean
		Get
			Return Me.AppSettingBoolean
		End Get
		Set
			Me.AppSettingBoolean = Value
		End Set
	End Property

#End Region

#Region " DATABASE SETTINGS "

	''' <summary> Gets the full pathname of the file. </summary>
	''' <value> The full pathname of the file. </value>
	Friend ReadOnly Property FilePathName As String
		Get
			Return System.IO.Path.Combine(My.Application.Info.DirectoryPath, Me.FileName)
		End Get
	End Property

	''' <summary> Gets the connection string. </summary>
	''' <value> The connection string. </value>
	Friend ReadOnly Property ConnectionString As String
		Get
			Return $"Filename={FilePathName}"
		End Get
	End Property

	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
	 Global.System.Configuration.DefaultSettingValueAttribute("\LiteDB\Products.db")>
	Public Property FileName() As String
		Get
			Return Me.AppSettingValue
		End Get
		Set
			Me.AppSettingValue = Value
		End Set
	End Property

	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
	 Global.System.Configuration.DefaultSettingValueAttribute("'Categories','Products','Sales',SalesDetails'")>
	Public Property SheetNames() As String
		Get
			Return Me.AppSettingValue
		End Get
		Set
			Me.AppSettingValue = Value
		End Set
	End Property

	Dim _SheetNameValues As ObjectModel.Collection(Of String)
	Public ReadOnly Property SheetNameValues As ObjectModel.Collection(Of String)
		Get
			If Me._SheetNameValues Is Nothing Then
				Me._SheetNameValues = New ObjectModel.Collection(Of String)(New List(Of String)(Me.SheetNames.Split(","c)))
			End If
			Return Me._SheetNameValues
		End Get
	End Property

	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
	 Global.System.Configuration.DefaultSettingValueAttribute("Select [CategoryName] from [Categories] Where ID=1;")>
	Public Property CategoryQuery() As String
		Get
			Return Me.AppSettingValue
		End Get
		Set
			Me.AppSettingValue = Value
		End Set
	End Property

	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
	 Global.System.Configuration.DefaultSettingValueAttribute("Air Filters")>
	Public Property CategoryResult() As String
		Get
			Return Me.AppSettingValue
		End Get
		Set
			Me.AppSettingValue = Value
		End Set
	End Property

	<Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
	 Global.System.Configuration.DefaultSettingValueAttribute("6")>
	Public Property CategoriesRowCount() As Integer
		Get
			Return Me.AppSettingInt32
		End Get
		Set
			Me.AppSettingInt32 = Value
		End Set
	End Property

#End Region

End Class
