Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports FluentAssertions
Imports System.Linq
Imports LiteDB

''' <summary>
''' Contains all unit tests for the <see cref="LiteDB"></see>
''' class.
''' </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/21/2019 </para></remarks>
<TestClass()>
Public Class LiteDbScribeTest2

#Region " CONSTRUCTION and CLEANUP "

	''' <summary> My class initialize. </summary>
	''' <param name="testContext"> Gets or sets the test context which provides information about
	'''                            and functionality for the current test run. </param>
	''' <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
	<CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
	<ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
        If _TestSite IsNot Nothing Then _TestSite.Dispose() : _TestSite = Nothing
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset).Split} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

	''' <summary> Cleans up after each test has run. </summary>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	'''<summary>
	'''Gets or sets the test context which provides
	'''information about and functionality for the current test run.
	'''</summary>
	Public Property TestContext() As TestContext

	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region "  STORE and SEARCH "

	''' <summary> A POCO entity, must have Id. </summary>
	''' <remarks>
	''' POCO class are simple C# classes using only get/set properties. It's the best way to create a
	''' strong typed documents. Your class must have an identifier property. You can use Id named
	''' property, or decorate any property with [BsonId] attribute.
	''' <list type="bullet"><item>
	''' Internal, document id is represent as "_id" property</item><item>
	''' Do Not use complex data types (Like DataSet, DataTable)</item><item>
	''' Do Not use disposable objects (Like Stream, Graphics)</item><item>
	''' Enums will be converted In strings When serialized</item><item>
	''' Your Id value must be a unique And Not null</item><item>
	''' If you leave Id empty, LiteDB will auto generate On insert.</item></list>
	''' </remarks>
	''' <remarks> David, 6/24/2019 </para></remarks>
	Public Class Customer
		' Public Property Id() As Integer
		<BsonId>
		Public Property AutoId() As Integer
		Public Property Name() As String
		Public Property Phones() As List(Of Phone)
		Public Property Sallary As Integer
		Public Property LastName() As String
	End Class

	''' <summary> It's not a entity, don't need Id </summary>
	Public Class Phone
		Public Property Code() As Integer
		Public Property Number() As String
		Public Property Type() As PhoneType
	End Class

	Public Enum PhoneType
		Mobile
		Landline
	End Enum

	''' <summary> A quick example for storing and searching documents. </summary>
	<Obsolete("not tested")>
	Public Sub StoreAndSearchTest()

		' Open database (or create if doesn't exist)
		Using db As New LiteDatabase("MyData.db")

			' Get customer collection
			Dim col As LiteCollection(Of Customer) = db.GetCollection(Of Customer)("customers")

			' Create your new customer instance
			Dim customer As New Customer With {.Name = "John Doe", .Phones = New List(Of Phone)({New Phone() With {.Code = 424, .Number = "123-4567", .Type = PhoneType.Mobile},
																								 New Phone() With {.Code = 310, .Number = "123-4567", .Type = PhoneType.Landline}})}

			' Create unique index in Name field
			col.EnsureIndex(Function(x) x.Name, True)

			' Insert new customer document (Id will be auto-incremented)
			col.Insert(customer)

			' Update a document inside a collection
			customer.Name = "Joana Doe"

			col.Update(customer)

			' Use LINQ to query documents (with no index)
			Dim results = col.Find(Function(x) x.Name.StartsWith("jo", StringComparison.OrdinalIgnoreCase))

		End Using

	End Sub

#End Region

#Region " DB REFERENCE "

	''' <summary> An order entity. </summary>
	Public Class Order
		Public Property OrderId() As Integer
		Public Property Customer() As Customer
	End Class

	''' <summary> Tests database reference. </summary>
	''' <remarks>
	''' LiteDB can use cross document references to map an inner entity using only _id value (not
	''' embedding hole sub document). LiteDB will serialize Order document as `{ _id: 123, Customer:
	''' { $id:1, $ref: "customers" } }`. When you need query Order including Customer instance, just
	''' add before run Find method.
	''' </remarks>
	<Obsolete("not tested")>
	Public Sub DatabaseReferenceTest()

		' Map Customer property to "customers" collections 
		BsonMapper.Global.Entity(Of Order)().DbRef(Function(x) x.Customer, "customers")

		' When insert an order, only customer _id will be included as reference

		' Open database (or create if doesn't exist)
		Using db As New LiteDatabase("MyData.db")

			' Create your new customer instance
			Dim customer As New Customer With {.Name = "John Doe", .Phones = New List(Of Phone)({New Phone() With {.Code = 424, .Number = "123-4567", .Type = PhoneType.Mobile},
																								 New Phone() With {.Code = 310, .Number = "123-4567", .Type = PhoneType.Landline}})}

			' Get customer collection
			Dim customers As LiteCollection(Of Customer) = db.GetCollection(Of Customer)("customers")

			' Create unique index in Name field
			customers.EnsureIndex(Function(x) x.Name, True)

			' Insert new customer document (Id will be auto-incremented)
			customers.Insert(New Customer With {.Name = "John Doe", .Phones = New List(Of Phone)({New Phone() With {.Code = 424, .Number = "123-4567", .Type = PhoneType.Mobile},
																							New Phone() With {.Code = 310, .Number = "123-4567", .Type = PhoneType.Landline}})})

			Dim orders As LiteCollection(Of Order) = db.GetCollection(Of Order)("orders")
			orders.Insert(New Order With {.OrderId = 1, .Customer = customers.FindOne(Function(x) String.Equals(x.Name, "John Doe", StringComparison.OrdinalIgnoreCase))})

			' Get orders collection
			orders = db.GetCollection(Of Order)("orders")

			' Find an order including Customer reference
			Dim order = orders.Include(Function(x) x.Customer).FindById(1)

		End Using


	End Sub

#End Region

#Region " COLLECTIONS "

	''' <summary> Tests collections. </summary>
	''' <remarks>
	''' LiteDB organize documents in stores (called in LiteDB as collections). Each collection has a
	''' unique name and contains documents with same schema/type. You can get a strong typed
	''' collection or a generic BsonDocument collections, using GetCollection from LiteDatabase
	''' instance. BsonDocument is a special class that maps any document with a internal
	''' Dictionary(of String, object). Is very useful to read a unknown document type or use as a
	''' generic document.
	''' <list type="bullet">
	''' Collection contains all method to manipulate documents: <item>
	''' Insert - Insert a New document</item><item>
	''' FindById, FindOne Or Find - Find a document using Query object Or LINQ expression. At this
	''' point, only simple LINQ are supported - attribute on left, value on right side.</item><item>
	''' Update - Update a document</item><item>
	''' Delete - Delete a document id Or using a query</item><item>
	''' Include - Use include to populate properties based on others collections</item><item>
	''' EnsureIndex - Create a index if Not exists. All queries must have a index.</item></list>
	''' </remarks>
	<Obsolete("not tested")>
	Public Sub CollectionsTest()

		Using db As New LiteDatabase(LiteDbScribeTestInfo.Get.ConnectionString)

			' Get a strong typed collection
			Dim customers As LiteCollection(Of Customer) = db.GetCollection(Of Customer)("Customers")

			' Get a BsonDocument collection 
			Dim customers2 As LiteCollection(Of BsonDocument) = db.GetCollection("Customers")

			' Create a BsonDocument for Customer with phones
			Dim doc As New BsonDocument()
			doc("_id") = ObjectId.NewObjectId()
			doc("Name") = "John Doe"
			doc("Phones") = New BsonArray
			Dim a As New BsonArray
			Dim value As New BsonValue()

			'value("Code") = 55
			'value("Phones")(0)("Number") = "(51) 8000-1234"
			'value("Phones")(0)("Type") = "Mobile"
			a.Add(value)

		End Using
	End Sub

#End Region

#Region " QUERY "

	''' <summary> Tests queries. </summary>
	''' <remarks>
	''' <list type="bullet">
	''' : <item>
	''' </item><item>
	''' </item></list>
	''' </remarks>
	<Obsolete("not tested")>
	Public Sub QueryTest()

		Using db As New LiteDatabase(LiteDbScribeTestInfo.Get.ConnectionString)

			Dim customers = db.GetCollection(Of Customer)("customers")

			' Create a new index (if not exists)
			customers.EnsureIndex("Name")

			' Query documents using 'Name' index
			Dim results As IEnumerable(Of Customer) = customers.Find(Query.StartsWith("Name", "John"))

			' Or using Linq
			results = customers.Find(Function(x) x.Name.StartsWith("John"))

			' Return document by ID (PK index)
			Dim customer = customers.FindById(1)

			' Count documents using Query helper
			Dim count = customers.Count(Query.GTE("Age", 22))

			' in memory query -  two indexed queries
			' 
			' All query results returns an IEnumerable<T>, so you can use Linq in results
			results = customers.Find(Function(x) (x.Name.StartsWith("John") AndAlso x.Sallary > 500)).
														 Where(Function(x) x.LastName.Length > 5).OrderBy(Function(x) x.Name)

		End Using
	End Sub

#End Region

#Region " FILE STORAGE "

	<Obsolete("not tested")>
	Public Sub StoreFileTest()

		Using db As New LiteDatabase(LiteDbScribeTestInfo.Get.ConnectionString)

			' Storing a file stream inside database
			db.FileStorage.Upload("my_key.png", "my_key.png")

			' Get file reference using file id
			Dim file As LiteFileInfo = db.FileStorage.FindById("my_key.png")

			' Find all files using StartsWith
			Dim files = db.FileStorage.Find("my_")

			' Get file stream
			Dim stream As System.IO.Stream = file.OpenRead()

			' Write file stream in a external stream
			file = db.FileStorage.Download("my_key.png", stream)

			' download into a file by name
			' file = db.FileStorage.Download("my_key.png", "c:\users\public\documents\my_key.png")

		End Using

	End Sub

#End Region

#Region " TRANSACTIONS "

	''' <summary> Tests transaction. </summary>
	''' <remarks>
	''' LiteDB has internal transactions (even in v4) but not expose to user to avoid keep opened. So,
	''' if engine has a fixed list of items to insert/update/delete, all operations can be done in a
	''' single transaction. But, my "problem" now Is that I read And understand more about WAL (Write
	''' Ahead Logging). All major databases implement this technique (SQL server, Postgres, Sqlite,
	''' MongoDB, ...) And are great to work with transactions. WAL support multiple write threads And
	''' implement atomicity And durability So, I'm implementing WAL in LiteDB and BeginTrans() will
	''' be back in next major version.
	''' </remarks>
	<Obsolete("Awaiting LiteDB version 5")>
	Public Sub TransactionTest()

	End Sub
#End Region

End Class
