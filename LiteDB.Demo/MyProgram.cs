using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using LiteDB;

namespace litedb_test
{
    internal class Program
    {
        public class EntityInt { public int Id { get; set; } public string Name { get; set; } }

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The args.</param>
        [STAThread]
        static void Main(string[] args)
        {
            var r = new { name = "John", Age = 40 };

            var d = BsonMapper.Global.ToDocument(r);

            var j = JsonSerializer.Serialize(d);

            string databaseName = @"test.db";
            string filename = $"c:\\users\\public\\documents\\{databaseName }";
            if (File.Exists(filename))
            {
                Console.WriteLine($"Deleting {filename}");
                File.Delete(filename);
            }

            Console.WriteLine($"Creating a new database {filename}");
            var db = new LiteDatabase(filename);
            var col = db.GetCollection<EntityInt>("col1");
            int recordCount = 50;
            Console.WriteLine($"Inserting {recordCount} columns into {databaseName}");
            for (int i = 0; i < recordCount; i++)
                col.Upsert(new EntityInt { Name = i.ToString() });

            int deleteCount = 10;
            Console.WriteLine($"Deleting {recordCount} columns from {databaseName}");
            for (int i = 0; i < deleteCount; i++)
                col.Delete(i);

            Console.WriteLine($"Shrinking {databaseName}");
            db.Shrink();

            int newCount = 5;
            Console.WriteLine($"Inserting {newCount} new records {databaseName}");
            for (int i = 0; i < newCount; i++)
                col.Upsert(new EntityInt { Name = i.ToString() }); //Cannot insert duplicate key in unique index '_id'. The duplicate value is '42'.
            Console.WriteLine($"Done working on {databaseName}; press any key to exit");
            Console.ReadKey();

        }
    }


}